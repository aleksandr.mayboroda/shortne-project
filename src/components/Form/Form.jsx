import {useEffect} from 'react'
import {useForm} from 'react-hook-form'
import {Button} from 'components/Button';

import classes from './Form.module.scss';

import { useSelector, useDispatch } from 'react-redux';
import { linksSelectors, linksOperations } from 'store/links';

const Form = () => {
  const dispatch = useDispatch()
  const isLoading = useSelector(linksSelectors.getIsLoading())
  const linksData = useSelector(linksSelectors.getData())

  const {
    register,
    formState,
    handleSubmit,
    setError,
    reset,
  } = useForm({mode: 'onSubmit'})

  const {errors} = formState

  const onSubmit = data => {
    if(linksData.length > 0)
    {
      const isExistedUri = linksData.find(elem => elem.original_link === data.url)
      console.log('existed',isExistedUri)
      if(isExistedUri)
      {
        setError("url", {
          type: "manual",
          message: `Url ${data.url} is already exist`,
        });
        return
      }
    }
    dispatch(linksOperations.createShortLink(data))
  }

  useEffect(() => {
    if (formState.isSubmitSuccessful) {
      console.log('reset')
      reset({ url: '' });
    }
  }, [formState, reset]);

  return (
    <section className={classes.section}>
      <div className="container">
        <form
          className={classes.form}
          autoComplete='off'
          onSubmit={handleSubmit(onSubmit)}
        >
          <input
            type="url"
            placeholder="Shorten a link here..."
            className={classes.input}
            {...register('url', {
              required: "You forgot to enter the link",
              pattern: {
                value: /^(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g,
                message: 'Don\'t be foolish!'
              }
            })}
            style={{
              outlineColor: errors.url ? 'var(--secondary-300)' : 'currentColor',
              outlineWidth: errors.url ? '4px' : '1px',
            }}
            disabled={isLoading}
          />
          <Button
            variant="square"
            type="submit"
            size="medium"
          >Shorten it!</Button>
          {errors.url && (
            <div className={classes.error}>
              {errors.url.message}
            </div>
          )}
        </form>
      </div>
    </section>
  )
}

export {Form};
