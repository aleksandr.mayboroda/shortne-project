import { useState } from 'react';
import {Button} from 'components/Button';
import {motion, AnimatePresence} from 'framer-motion'

import { useSelector } from 'react-redux';
import { linksSelectors } from 'store/links';

import classes from './Shortens.module.scss';

const Shortens = () => {
    const links = useSelector(linksSelectors.getData());
    const [isCopied, setIsCopied] = useState(null);

    const copyToClipboard = link => {
        navigator.clipboard.writeText(link).then(() => setIsCopied(link )) //copy to clipboard === ctrl + c
    }

    if (!links?.length) return null;

    return (
        <section className={classes.Shortens}>
            <div className='container'>
                {[...links].reverse().map(item => (
                    <AnimatePresence
                        key={item.code}
                    >
                        <motion.div
                            className={classes.item}
                            initial={{opacity: 0, height: 0}}
                            animate={{opacity: 1, height: 'auto'}}
                            data-active={isCopied === item.full_short_link2}
                        >
                            <span>{item.original_link}</span>
                            <span>{item.full_short_link2}</span>
                            <Button
                                variant="square"
                                onClick={() => copyToClipboard(item.full_short_link2)}
                            >
                                {isCopied === item.full_short_link2 ? 'Copied' : 'Copy'}
                            </Button>
                        </motion.div>
                    </AnimatePresence>
                ))}
            </div>
        </section>
    );
};

export {Shortens};
