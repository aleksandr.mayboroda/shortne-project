import axios from 'axios'

const BASE_URL = 'https://api.shrtco.de/v2/shorten'

export const createUrl = url => axios(BASE_URL, {params: url})