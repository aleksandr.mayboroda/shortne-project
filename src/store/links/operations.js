import {actions, createShortLink} from './slice'

const def = {
  ...actions,
  createShortLink,
}

export default def