const getData = () => state => state.links.data
const getIsLoading = () => state => state.links.isLoading

const def = {
  getData, getIsLoading,
}

export default def