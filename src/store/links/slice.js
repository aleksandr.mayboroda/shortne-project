import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import {createUrl} from 'api/link'

export const createShortLink = createAsyncThunk(
  'links/createShortLink',
  async url => {
    try{
      const response = await createUrl(url)
      return response.data
    }
    catch(err)
    {
      console.log('MY err happened in link', err)
    }
  }
)

const initialState = {
  data: [],
  isLoading: false,
}

const linkSlice = createSlice({
  name: 'links',
  initialState,
  extraReducers: {
    [createShortLink.rejected]: (state) => {
      state.isLoading = false
    },
    [createShortLink.pending]: (state) => {
      state.isLoading = true
    },
    [createShortLink.fulfilled]: (state, action) => {
      state.isLoading = false
      const {ok, result} = action.payload 
      if(ok)
      {
        state.data.push(result)
      }
    },
  }
})

export const {actions} = linkSlice

export default linkSlice.reducer