// import {configureStore, combineReducers} from '@reduxjs/toolkit'
// import storage from 'redux-persist/lib/storage'
// import { 
//   persistStore,
//   persistReducer,
//   FLUSH,
//   REHYDRATE,
//   PAUSE,
//   PERSIST,
//   PURGE,
//   REGISTER
// } from 'redux-persist'
// import links from './links'

// const persistConfig = {
//   key: 'root',
//   storage,
// }

// const rootReducer = combineReducers({ //без combineReducers не заработает
//   links,
// })

// const persReducer = persistReducer(persistConfig, rootReducer)

// export const store = configureStore({
//   reducer: persReducer, //rootReducer
//   middleware: (getDefaultMiddleware) => getDefaultMiddleware({ //это как в финальном проекте, чтобы не ругался redux-toolkit
//     serializableCheck: {
//       ignoredActions: [
//         FLUSH,
//         REHYDRATE,
//         PAUSE,
//         PERSIST,
//         PURGE,
//         REGISTER,
//       ],
//     },
//   })
// })



// export const persistor = persistStore(store)
// // export default store


import {configureStore, combineReducers} from '@reduxjs/toolkit'
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import linksReducer from './links'

const persistConfig = {
  key: 'root',
  storage,
  
}

const rootReducer = combineReducers({
  links: linksReducer,
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [
        FLUSH,
        REHYDRATE,
        PAUSE,
        PERSIST,
        PURGE,
        REGISTER,
      ]
    }
  })
})

export const persistor = persistStore(store)