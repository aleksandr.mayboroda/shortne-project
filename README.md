# Frontend Mentor - Shortly URL shortening API Challenge solution

Вариант решения задачки [Shortly URL shortening API Challenge challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/url-shortening-api-landing-page-2ce3ob-G). Версия для видео с YouTube-канала Михаила Непомнящего. 

Описание:
На странице есть форма для создания коротких ссылок. Для этой формы реализована валидация при помощи регулярного выражения.
Для создания ссылки используется API сервиса https://shrtco.de/docs/

Использованы технологии:
- react
- redux-toolkit
- redux-persist
- axios
- react-hook-form
- framer-motion

Дополнительно сделано:
- проверка url на существование в редаксе;
- очистка поля плосле добваления элемента;
- редакс написан по своей схеме - импорт reducer, actions & operations из файла ./slice_dir/index.js
